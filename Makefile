CC             := gcc
CFLAGS         := $(if $(DEBUG),-g -O0,-O3) -c -Wall

CFILES   := csvparse.c divide.c
BINARIES := csvparse divide

all: csvparse divide

csvparse: csvparse.o
	$(CC) -o $@ $<

divide: divide.o
	$(CC) -o $@ $<

%.o: %.c
	$(CC) $(CFLAGS) $<

clean:
	rm -f $(CFILES:.c=.o) $(BINARIES)

# vim:set noet sts=0 sw=4 ts=4 tw=120:

