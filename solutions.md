# Solutions

## 1 - Fichiers et affichage formaté

Voir le fichier `csvparse.c`.

## 2 - Commandes d'une ligne


>1. (man, head) Sauvegardez les 20 premières lignes du "manuel d'instructions"
>   de la fonction `printf` dans un fichier nommé `man-printf.txt`.

```sh
$ man printf | head -n 20 >man-printf.txt
```

>2. (curl, grep) Affichez toutes les lignes qui contiennent un commentaire dans
>   le fichier `array.c` disponible dans le répertoire `exemples` (ne recopiez
>   pas le fichier sur votre machine, utilisez le programme curl!).

Rendez-vous sur https://gitlab.com/sim590/inf3135-exercices/ et sélectionnez le
fichier `array.c` dans le répertoire `exemples`. Ensuite, il est possible
d'ouvrir le fichier en format `brut`. Une fois que c'est fait, récupérez l'URL
de ce fichire brut et entrez la commande:

```sh
$ curl https://gitlab.com/sim590/inf3135-exercices/raw/master/exemples/array.c | grep '//\|/\*\|\*/\|^\s*\*'
```

Les différentes expressions chaînées par des OU logiques `\|` dans la chaîne de caractères `//\|/\*\|\*/\|^\s*\*` sont décrites ci-après:

- `//`: deux barres obliques consécutives;
- `/\*`: une barre oblique suivi d'une étoile. Le caractère `*` est réservé
  dans les expressions régulières. Il faut donc l'échaper d'une barre oblique
  inversée;
- `\*/`: une étoile suivi d'une barre oblique;
- `^\s*\*`: `^` est le début de la ligne, `\s*` signifie un caractère
  d'espacement répété 0 ou plus de fois (`*`) et finalement `\*` est un
  caractère d'étoile. Le tout indique donc une ligne qui commence par une
  étoile précédée d'espaces vides seulement. **Noter**: il est possible qu'un
  commentaire de la forme `/*<cr>*<texte><cr>*/` contienne des caractères entre
  les étoiles de début de ligne intermédiaires, et donc ne serait pas pris en
  charge par notre expression régulière. Cependant, il est contraire au style et
  très improbable que cette situation soit rencontrée.

>3. (git log, grep, sort, uniq) Affichez tous les contributeurs d'un projet
>   versionné avec git. Par exemple, j'obtiens le résultat suivant pour le
>   projet [TMX](https://github.com/baylej/tmx), que j'ai utilisé par le passé:
>
>    ```
>    Author: Alex Tennant <adtennant@gmail.com>
>    Author: Alex Tennant <alex.tennant@desynit.com>
>    Author: Alex Tennant <alte@Alexs-MBP.home>
>    Author: Alexandre Blondin Massé <alexandre.blondin.masse@gmail.com>
>    Author: Bayle Jonathan <bayle.jonathan@gmail.com>
>    Author: Bayle Jonathan <baylej@mrhide.fr>
>    Author: RPG Hacker <markus_wall@web.de>
>    Author: U-hide-hard\hide <hide@hide-hard.(none)>
>    Author: baylej <baylej@mrhide.fr>
>    Author: baylej <hide@hide-hard.(none)>
>    Author: pedrohlc <pedro.laracampos@gmail.com>
>    ```

```sh
$ git log | grep -e '^Author: ' | sort | uniq
```

On récupère les lignes commançant par la chaîne de caractères `Author: `.
Ensuite, on les tri de façon à ce que `uniq` puisse retirer les redondances
(voir `man 1 uniq`).

>4. (git log, grep, sed, sort, uniq) Modifiez la commande précédente pour
>   obtenir la liste sans prénom et nom en double et sans le préfixe "Author:
>   ". Le résultat deviendrait donc (il y en a encore des doublons, puisque
>   "Bayle Jonathan" et "baylej" sont sans doute les mêmes personnes, mais
>   nous ne traiterons pas ce cas) :
>
>    ```
>    Alex Tennant
>    Alexandre Blondin Massé
>    Bayle Jonathan
>    RPG Hacker
>    U-hide-hard\hide
>    baylej
>    pedrohlc
>    ```

```sh
$ git log | grep -e '^Author: ' | sed -e 's,Author: ,,' -e 's,<.*>,,' | sort | uniq
```

L'ajout ici, est le filtre avec le programme `sed`. On utilise les options `-e`
afin de fournir plusieurs expressions de filtre (voir `man 1 sed`). Les
expressions sont décrites ci-après:

- `sed -e 's,toto,titi,g'`: la lettre `s` indique que la commande est une
  recherche et remplacement de motif. Dans cet exemple, on recherche `toto` à
  remplacer par `titi`. Les virgules `,` sont choisies arbitrairement afin de
  délimiter les expressions. Une commande équivalente serait `sed -e 's/toto/titi/g'`.
  On choisit les virgules lorsqu'on souhaite éviter les conflits avec les
  chemins UNIX par exemple: `/home/simon/Documents`.
- `'s,Author: ,,'`: remplacement de la chaîne de caractères `Author: ` par la
  chaîne vide. C'est donc une suppression de l'expression.
- `'s,<.*>,,' `: remplacement d'une expression commençant par un chevron ouvrant
  `<`, contenant suivie de n'importe quoi `.` un nombre de fois de 0 ou plus `*`
  et terminé par un chevron fermant `>`. Ceci retire donc les adresses courriels
  du texte initial.

## 3 - Graphviz

Voir le fichier `divide.c`.

