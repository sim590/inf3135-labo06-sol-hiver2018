#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#define BUF_LEN 1024

/* Dit si la chaîne de caractères possède exactement deux virgules
 */
bool has_two_commas(const char* s) {
    const char* j = s;
    for (int i = 0; i < 2; ++i) {
        j = index(j, ',');
        if (!j++) return false;
    }
    return !index(j, ',');
}

/* Taille la chaîne de caractère afin d'y retirer toutes les occurences du
 * caractère `c`
 */
char* trim(const char* s, char c) {
    char *rs = malloc(strlen(s)*sizeof(char) + 1);
    strcpy(rs, s);
    int j = 0;
    for (int i = 0; i < strlen(s); ++i) {
        if (s[i] != c) {
            rs[j] = s[i];
            ++j;
        }
    }
    rs[j] = '\0';
    return rs;
}

int main(int argc, char *argv[]) {
    char buf[BUF_LEN];
    printf("Rang  Nom              Pays  Population\n");
    printf("----  ---              ----  ----------\n");
    unsigned int rank = 0;

    while (fgets(buf, BUF_LEN, stdin)) {
        /* si la ligne ne contient pas exactement deux virgules, on doit la rejeter. */
        if (!has_two_commas(buf)) continue;

        char* name = trim(strtok(buf, ","), '\n');
        char* country = trim(strtok(NULL, ","), '\n');
        char* population = trim(strtok(NULL, ","), '\n');

        /*
         * %04d: (voir man 3 printf) Insérer 4 0s en préfixe du nombre
         *  %*s: (voir man 3 printf) Insère un nombre d'espace donné
         * %.4s: (voir man 3 printf) Afficher maximum 4 caractères de la chaîne
         *       de caractères.
         */
        printf("%04d  %s%*s  %.4s  %*s%s\n",
                ++rank,
                name,
                (int)(15-strlen(name)), "", // ces deux arguments sont lirés par %*s
                country,
                (int)(10-strlen(population)), "", // ces deux arguments sont lirés par %*s
                population);

        /* Les lignes suivantes sont nécessaires. Pouvez-vous dire pourquoi?
         * Voir comment name, country et population sont créés (déf. de trim).
         */
        free(name); free(country); free(population);
    }
    return 0;
}

/* vim:set et sw=4 ts=4 tw=120: */

