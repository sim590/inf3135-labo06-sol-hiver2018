#include <stdio.h>

int main(int argc, char *argv[]) {
    printf("digraph G {\n");
    for (int i = 1; i < 49; ++i)
        printf("%i\n", i);
    for (int i = 1; i < 49; ++i) {
        for (int j = 1; j < 49; ++j) {
            if (j % i == 0)
                printf("%i -> %i\n", i, j);
        }
    }
    printf("}\n");
    return 0;
}
